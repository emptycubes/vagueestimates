/**
 * Created by Jack on 7/21/2014.
 */
/// <reference path="../jquery/jquery.d.ts" />

interface IDropDownOptions {
    other:boolean;
    listSize:number;
    maxHeight:number;
    emptyText:string;
    allowEmpty:boolean;
    ignoreEmptyOptions:boolean;
    emptyOption:string;
    filterEx:string;
    filterOpt:string;
    duration:number;
    openOnFocus:boolean;
    viewPort:HTMLElement;
}

interface JQuery {
    DropDown(options?:IDropDownOptions);
    DropDown(method?:string);
}
