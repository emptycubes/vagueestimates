/**
 * Created by Jack on 6/29/2014.
 */

interface PromiseStatic {
    new(deffered:(resolve:Function, reject:Function) => void):IPromise

    reject(reason:Error):IPromise;
    resolve(value:any):IPromise;
    all(promises:Array<IPromise>):IPromise;
    race(promises:Array<IPromise>):IPromise;
}

interface IPromise {
    then(fulfilled:Function, rejected?:Function):IPromise;
    catch(rejected:Function):IPromise;
}

declare var Promise:PromiseStatic;
