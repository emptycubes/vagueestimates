/**
 * Created by Jack on 6/29/2014.
 */

declare module libParse {
    export interface IPromiseStatic {
        new():IPromise;
        error(message:string):IPromise;
        as(value:any):IPromise;
        when(promises:Array<IPromise>):IPromise;
    }

    export interface IPromise {
        resolve(value:any);
        reject(error:string);
        then(success:Function, failed?:Function):IPromise;
    }

    export interface IObjectPromise extends IPromise {
        success(results:IObject):void;
        error(results:IObject, error:Error):void;
    }

    export interface IQueryPromise extends IPromise {
        success(results:Array<IObject>):void;
        error(results:Array<IObject>, error:Error):void;
    }

    export interface ICountPromise extends IPromise {
        success(count:number):void;
        error(error:Error):void;
    }

    export interface IObjectStatic {
        new();
        new(className:string):IObject;

        extend(className?:string, proto?:Object, staticProto?:Object):IObjectStatic;
        destroyAll(list:Array<IObject>, options:any):void;
        fetchAll(list:Array<IObject>, options:any):void;
        fetchAllIfNeeded(list:Array<IObject>, option:any):void;
        saveAll(list:Array<IObject>, option:any):void;
    }

    export interface IObject {
        [fieldName:string]:any;

        id:string;
        createdAt:Date;
        updatedAt:Date;

        get(fieldName:string):any;
        set(fieldName:string, value:any):void;
        unset(fieldName:string):void;


        fetch(promise:IObjectPromise):IPromise;
        save(values?:Object, promise?:IObjectPromise):IPromise;
        destroy(promise?:IObjectPromise):void;


        increment(fieldName:string, step?:number):void;

        add(fieldName:string, value:any):void;
        addUnique(fieldName:string, value:any):void;

        relation(fieldName:string):IRelation;
    }

    //TODO: Does this implement an ICollection that could be
    //      used for internal arrays on IObject?
    export interface IRelation {
        add(model:IObject):void;
        add(model:Array<IObject>):void;
        remove(model:IObject):void;

        //TODO: Should this be in IObject?
        query():IQuery;
    }

    export interface IQueryStatic {
        new(className:string):IQuery;
        new(model:IObject):IQuery;
        new(model:IObjectStatic):IQuery;

        or(first:IQuery, second:IQuery):IQuery;
    }

    export interface IQuery {
        get(id:string, promise?:IObjectPromise):IPromise;
        find(promise?:IQueryPromise):IQueryPromise;
        first(promise?:IObjectPromise):IQueryPromise;

        exists(fieldName:string):IQuery;
        doesNotExist(fieldName:string):IQuery;
        equalTo(fieldName:string, value:any):IQuery;
        notEqualTo(fieldName:string, value:any):IQuery;
        greaterThan(fieldName:string, value:any):IQuery;
        greaterThanOrEqualTo(fieldName:string, value:any):IQuery;
        lessThan(fieldName:string, value:any):IQuery;
        lessThanOrEqualTo(fieldName:string, value:any):IQuery;
        containedIn(fieldName:string, values:Array<any>):IQuery;
        notContainedIn(fieldName:string, values:Array<any>):IQuery;
        containsAll(fieldName:string, values:Array<any>):IQuery;
        startsWith(fieldName:string, value:string):IQuery;

        matchesQuery(fieldName:string, query:IQuery):IQuery;
        doesNotMatchQuery(fieldName:string, query:IQuery):IQuery;
        matchesKeyInQuery(outerKey:string, innerKey:string, innerQuery:IQuery):IQuery;
        doesNotMatchKeyInQuery(outerKey:string, innerKey:string, innerQuery:IQuery):IQuery;

        ascending(fieldName:string):IQuery;
        descending(fieldName:string):IQuery;

        limit(count:number):IQuery;
        skip(count:number):IQuery;
        count(promise:ICountPromise):IQuery;

        select(...fieldName:Array<string>):IQuery;
        include(fieldName:string):IQuery;
        include(fieldName:Array<string>):IQuery;

        collection():ICollection;
    }

    export interface IUserStatic {
        new():IUser;
        current():IUser;
    }

    export interface IUser extends IObject {
        UserName:string;
    }

    export interface ICollectionStatic {
        extend(something:any):ICollection;
    }

    export interface ICollection {
        comparator:(model:IObject) => any;

        fetch(promise:IQueryPromise):void;
        add(model:IObject):void;
        add(models:Array<IObject>):void;
        get(id:string):void;
        remove(model:IObject):void;
        reset(models:Array<IObject>):void;
    }

    export interface IParse {
        Object:IObjectStatic;
        Query:IQueryStatic;
        Collection:ICollection;

        initialize(applicationId:string, javaScriptKey:string, masterKey?:string):void;
    }
}

declare var Parse:libParse.IParse;
