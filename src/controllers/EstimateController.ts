/**
 * Created by Jack on 6/28/2014.
 */
/// <reference path="AngularController.ts" />
/// <reference path="../interfaces/IEstimateService.ts" />
/// <reference path="../interfaces/IListService.ts" />

module VagueEstimates.Controllers {
    export class EstimateController extends AngularController {
        static $inject:Array<string> = [
            "$scope",
            "$routeParams",
            "estimateService",
            "$list"
        ];

        constructor($scope:ng.IScope,
                    $routeParams:Object,
                    private _service:Services.IEstimateService,
                    private _listService:Services.IListService) {

            super($scope, $routeParams);
            this.HandleAction($scope, $routeParams);
        }

        public index($scope:ng.IScope, $routeParams:Object):void {
            var pageNumber = parseInt($routeParams['page']) || 0,
                pageSize = parseInt($routeParams['pageSize']) || 25;


            this._service
                .listEstimates(pageNumber, pageSize)
                .catch(this.OnError)
                .then(results => $scope['results'] = results)
                .then(results => View("partials/estimate/list.html", $scope));
        }

        public create($scope:ng.IScope, $routeParams:Object):void {
            View("partials/estimate/editor.html", $scope);
        }

        public edit($scope:ng.IScope, $routeParams:Object):void {
            var estimateId = <string>$routeParams['estimateId'];

            this._service
                .getEstimateById(estimateId)
                .catch(this.OnError)
                .then(model => $scope['model'] = model)
                .then(() => {

                    //Load UnitsOfMeasure
                    this._listService
                        .getUnitsOfMeasure()
                        .catch(this.OnError)
                        .then(units => $scope['unitsOfMeasure'] = units);
                })
                .then(() => View("partials/estimate/editor.html", $scope));
        }

        public put($scope:ng.IScope, $routeParams:Object):void {
            if (!$routeParams['estimate']) {
                this.OnError(new Error("Unable to bind to Estimate model"));
                return;
            }

            //Bind viewmodel to model classes, would be nice if angular could
            //return our viewmodel type
            var viewModel = <Models.EstimateModel>
                $.extend(new Models.EstimateModel(), $routeParams['estimate']);

            //Don't forget about the team members
            viewModel.Members =
                viewModel.Members
                         .select(model => $.extend(new Models.TeamMemberModel(), model));

            this._service
                .saveEstimate(viewModel)
                .catch(this.OnError)
                .then(model => $scope['model'] = model)
                .then(model => View("partials/estimate/view.html", $scope));
        }

        public view($scope:ng.IScope, $routeParams:Object):void {
            var estimateId:string =
                $routeParams['estimateId'];

            this._service
                .getEstimateById(estimateId)
                .catch(this.OnError)
                .then(model => $scope['model'] = model)
                .then(model => View("partials/estimate/view.html", $scope));
        }
    }
}
