/**
 * Created by Jack on 6/23/2014.
 */

/// <reference path="AngularController.ts" />

module VagueEstimates.Controllers {
    export class HomeController extends AngularController {
        constructor($scope:ng.IScope, $routeParams:Object) {
            super($scope, $routeParams);
            console.debug("HomeController");
        }
    }
}
