/**
 * Created by Jack on 6/28/2014.
 */
/// <reference path="../../typings/angularjs/angular.d.ts" />

module VagueEstimates.Controllers {
    export function View(path:string, $scope:ng.IScope):void {
        $scope['view'] = path;
    }

    export class AngularController {
        static $inject:Array<string> = [
            "$scope",
            "$routeParams"
        ];

        constructor($scope:ng.IScope, $routeParams:Object) {}

        public HandleAction($scope:ng.IScope, $routeParams:Object) {
            var action = $routeParams['action'] || "index";

            if (typeof action !== 'undefined') {
                delete $routeParams['action'];
                this.OnExecuteAction(action, $routeParams, $scope);
            }
        }

        public OnExecuteAction(action:string, $routeParams:Object, $scope:any):void {
            if (typeof this[action] === 'function') {
                this[action]($scope, $routeParams);
            }
        }

        public OnError(error:Error) {
            //TODO: console log error?
            console.error(error);
        }
    }
}
