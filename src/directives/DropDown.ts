/**
 * Created by Jack on 7/21/2014.
 */
/// <reference path="../../typings/angularjs/angular.d.ts" />
/// <reference path="../../typings/jqueryui/DropDown.d.ts" />

module VagueEstimates {
    export function DropDown():ng.IDirective {
        return {
            link: (scope, element, attrs) => scope.$watch(attrs['source'], function() {
                var source = scope.$eval(attrs['source']),
                    options = scope.$eval(attrs['dropdown']);

                if (angular.isDefined(source)) {
                    element.DropDown(options);
                }
            })
        };
    }
}
