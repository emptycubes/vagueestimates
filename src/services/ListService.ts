/**
 * Created by Jack on 7/21/2014.
 */

/// <reference path="../interfaces/IListService.ts" />
/// <reference path="../../typings/parse/parse.d.ts" />
/// <reference path="../../typings/angularjs/angular.d.ts" />
/// <reference path="../adapters/UnitOfMeasureAdapter.ts" />

module VagueEstimates.Services {
    export class ListService implements IListService {
        static $inject:Array<string> = [
            "parse",
            "$q"
        ];

        //Caching so we aren't wasting time on service calls.
        static _unitsOfMeasure:Array<Models.UnitOfMeasure> = [];

        constructor(private _parse:libParse.IParse, private _q:ng.IQService) { }

        public getUnitsOfMeasure():ng.IPromise<Models.UnitOfMeasure[]> {
            var UnitOfMeasureEntity = this._parse
                                          .Object
                                          .extend(VagueEstimates.__UNITS_OF_MEASURE_ENTITY),
                query = new this._parse.Query(UnitOfMeasureEntity),
                deferred = this._q.defer();

            //No need to continue if we've already cached the units
            if (ListService._unitsOfMeasure.length) {
                deferred.resolve(ListService._unitsOfMeasure);
                return deferred.promise;
            }

            query.equalTo("Active", true)
                 .find()
                 .then(units => units.select(entity => Adapters.UnitOfMeasureAdapter.ToViewModel(entity)), deferred.reject)
                 .then(models => deferred.resolve(ListService._unitsOfMeasure = models));

            return deferred.promise;
        }

    }
}
