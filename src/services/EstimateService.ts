/**
 * Created by Jack on 6/29/2014.
 */
/// <reference path="../../typings/parse/parse.d.ts" />
/// <reference path="../../typings/angularjs/angular.d.ts" />
/// <reference path="../../typings/array/array.d.ts" />
/// <reference path="../interfaces/IEstimateService.ts" />
/// <reference path="../interfaces/IListService.ts" />
/// <reference path="../models/EstimateModel.ts" />
/// <reference path="../Adapters/EstimateAdapter.ts" />
/// <reference path="../Adapters/MemberAdapter.ts" />
/// <reference path="../app/Constants.ts" />

module VagueEstimates.Services {
    export class EstimateService implements IEstimateService {
        static $inject:Array<string> = [
            "parse",
            "$q",
            "$list"
        ];

        constructor(private _parse:libParse.IParse, private _q:ng.IQService, private _listService:IListService) { }

        public getEstimateById(id:string):ng.IPromise<Models.EstimateModel> {
            var entity = this._parse
                             .Object
                             .extend(VagueEstimates.__ESTIMATE_ENTITY),
                query = new this._parse.Query(entity),
                deferred = this._q.defer<Models.EstimateModel>();

            query.get(id)
                 .then(estimate => {
                    //Get our members and adapt
                    return estimate
                        .relation(VagueEstimates.__MEMBER_ENTITY)
                        .query()
                        .find()
                        .then(members => Adapters.EstimateAdapter.ToViewModel(estimate, members),
                              deferred.reject)
                        .then(model => estimate
                            .relation(VagueEstimates.__UNITS_OF_MEASURE_ENTITY)
                            .query()
                            .find()
                            .then(units => (model.Units = units.select(Adapters.UnitOfMeasureAdapter.ToViewModel)) && model)
                        );
                 })
                 .then(deferred.resolve, deferred.reject);

            return deferred.promise;
        }

        public listEstimates(pageNumber:number = 0, pageCount:number = 25):ng.IPromise<Models.EstimateModel[]> {
            var entity = this._parse
                             .Object
                             .extend(VagueEstimates.__ESTIMATE_ENTITY),
                query = new this._parse.Query(entity),
                deferred = this._q.defer<Models.EstimateModel[]>();


                //Starting to feel like a politician w/ all these promises.
                query.descending("updatedAt")
                    .skip(pageNumber * pageCount)
                    .limit(pageCount)
                    .find()
                    .then(estimates => {
                        var promises:Array<ng.IPromise<Models.TeamMemberModel[]>> = [];

                        //Get the related members and adapt them and it's parent
                        //estimate
                        estimates.forEach(estimate => {
                            var promise = estimate
                                .relation(VagueEstimates.__MEMBER_ENTITY)
                                .query()
                                .find()
                                .then(members => Adapters.EstimateAdapter.ToViewModel(estimate, members),
                                      deferred.reject);

                            promises.push(promise);
                        });

                        return this._q
                            .all(promises);
                    })
                    .then(deferred.resolve, deferred.reject);

            return deferred.promise;
        }

        //Saves or creates an estimate
        public saveEstimate(model:Models.EstimateModel):ng.IPromise<Models.EstimateModel> {
            //Create our entity classes
            var EstimateEntity = this._parse
                                     .Object
                                     .extend(VagueEstimates.__ESTIMATE_ENTITY),
                MemberEntity = this._parse
                                   .Object
                                   .extend(VagueEstimates.__MEMBER_ENTITY);

            //Map out member view models to parse objects
            var members = model
                .Members
                .select(member => Adapters.MemberAdapter.ToEntity(member, MemberEntity));

            //Map our entity to a parse object
            //then assign the members as relatives.
            var estimate = Adapters
                .EstimateAdapter
                .ToEntity(model, members, EstimateEntity);

            //Setup a deffered for the save function
            var deferred = this._q.defer<Models.EstimateModel>();

            //Do the save, and resolve/reject the deferred
            estimate.save()
                    .then(deferred.resolve, deferred.reject);

            return deferred.promise;
        }
    }
}
