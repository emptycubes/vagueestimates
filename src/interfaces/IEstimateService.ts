/**
 * Created by Jack on 6/29/2014.
 */
/// <reference path="../../typings/angularjs/angular.d.ts" />
/// <reference path="../models/EstimateModel.ts" />

module VagueEstimates.Services {
    export interface IEstimateService {
        getEstimateById(id:string):ng.IPromise<Models.EstimateModel>;
        listEstimates(pageNumber:number, pageCount:number):ng.IPromise<Models.EstimateModel[]>;
        saveEstimate(model:Models.EstimateModel):ng.IPromise<Models.EstimateModel>;
    }
}
