/**
 * Created by Jack on 7/21/2014.
 */
/// <reference path="../../typings/angularjs/angular.d.ts" />
/// <reference path="../models/UnitOfMeasure.ts" />

module VagueEstimates.Services {
    export interface IListService {
        getUnitsOfMeasure():ng.IPromise<Models.UnitOfMeasure[]>;
    }
}
