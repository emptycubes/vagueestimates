/**
 * Created by Jack on 6/22/2014.
 */

/// <reference path="../../typings/angularjs/angular.d.ts" />
/// <reference path="../../typings/angularjs/angular-route.d.ts" />
/// <reference path="../../typings/parse/parse.d.ts" />
/// <reference path="../services/EstimateService.ts" />
/// <reference path="../services/ListService.ts" />
/// <reference path="../controllers/EstimateController.ts" />
/// <reference path="../controllers/HomeController.ts" />
/// <reference path="../directives/DropDown.ts" />

module VagueEstimates {
    function configureRoute($routeProvider:ng.route.IRouteProvider):void {
        $routeProvider
            .when('/', {
                templateUrl: 'partials/home.html',
                controller: 'Home'
            })
            .when('/Estimate/:action?', {
                templateUrl: 'partials/estimate/index.html',
                controller: 'Estimate'
            })
            .otherwise({
                redirectTo: '/'
            });
    }

    angular
        .module('VagueEstimates', ['ngRoute'])
        .config(['$routeProvider', configureRoute])

       /******************
        * Services
        ******************/
        .factory('parse', [() => {
            Parse.initialize("9JYUSDWR1UvKRATLKHVtvjVbZbp5DfXoutsPvTID",
                             "yQd4l3MKQcMkuFXnf39vy2T4szohuwoe3d5RaZoE");

            return Parse; //Parse is really a ns, just hand it off
        }])
        .service("$list", Services.ListService)
        .service('estimateService', Services.EstimateService)

       /******************
        * Controllers
        ******************/
        .controller('Home', Controllers.HomeController)
        .controller('Estimate', Controllers.EstimateController)

       /******************
        * Controllers
        ******************/
        .directive('dropdown', VagueEstimates.DropDown);
}
