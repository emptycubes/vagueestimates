/**
 * Created by Jack on 7/11/2014.
 */

module VagueEstimates {
    export var __ESTIMATE_ENTITY:string = "Estimate";
    export var __MEMBER_ENTITY:string = "Members";
    export var __UNITS_OF_MEASURE_ENTITY:string = "Units";
}
