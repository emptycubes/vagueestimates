/**
 * Created by Jack on 6/12/2014.
 */
/// <reference path="../../typings/requirejs/require.d.ts" />

require.config({

    baseUrl: '../../',

    paths: {
        src: 'src/',
        lib: 'lib/',
        jquery:'VagueEstimates/lib/jquery/dist/jquery.min',
        angular:'VagueEstimates/lib/angular/angular.min',
        bootstrap: 'VagueEstimates/lib/bootstrap/dist/js/bootstrap'
    },

    shim: {
        jquery: { exports: '$' },
        angular: { exports: 'angular' },
        bootstrap: { exports: 'bootstrap' }
    }
});
