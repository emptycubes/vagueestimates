/**
 * Created by Jack on 6/29/2014.
 */
/// <reference path="TeamMemberModel.ts" />
/// <reference path="UnitOfMeasure.ts" />

module VagueEstimates.Models {
    export class EstimateModel {
        public EstimateId:string;
        public ProjectName:string;
        public ProductOwner:string;
        public Deadline:Date;
        public Members:Array<TeamMemberModel>;
        public Units:Array<UnitOfMeasure>;
    }
}
