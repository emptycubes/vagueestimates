/**
 * Created by Jack on 7/21/2014.
 */

module VagueEstimates.Models {
    export class UnitOfMeasure {
        public UnitId:string;
        public Text:string;
        public Description:string;
    }
}
