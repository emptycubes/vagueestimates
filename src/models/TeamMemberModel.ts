/**
 * Created by Jack on 6/29/2014.
 */

module VagueEstimates.Models {
    export class TeamMemberModel {
        public MemberId:string;
        public Name:string;
        public Roles:Array<string>;
        public WorkLoad:number;
    }
}
