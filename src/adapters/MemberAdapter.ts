/**
 * Created by Jack on 7/11/2014.
 */
/// <reference path="../../typings/parse/parse.d.ts" />
/// <reference path="../models/TeamMemberModel.ts" />

module VagueEstimates.Adapters {
    export class MemberAdapter {
        public static ToViewModel(entity:libParse.IObject):Models.TeamMemberModel {
            var teamMember = new Models.TeamMemberModel();

            teamMember.Name = entity.get("Name");
            teamMember.Roles = entity.get("Roles");
            teamMember.WorkLoad = entity.get("WorkLoad");

            return teamMember;
        }

        public static ToEntity(viewModel:Models.TeamMemberModel, entity:libParse.IObjectStatic):libParse.IObject {
            var member = new entity();

            member.id = viewModel.MemberId;
            member.set("Name", viewModel.Name);
            member.set("Roles", viewModel.Roles);
            member.set("WorkLoad", viewModel.WorkLoad);

            return member;
        }
    }
}
