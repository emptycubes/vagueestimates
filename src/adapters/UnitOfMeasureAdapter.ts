/**
 * Created by Jack on 7/21/2014.
 */
/// <reference path="../../typings/parse/parse.d.ts" />
/// <reference path="../models/UnitOfMeasure.ts" />

module VagueEstimates.Adapters {
    export class UnitOfMeasureAdapter {
        public static ToViewModel(entity:libParse.IObject):Models.UnitOfMeasure {
            var model = new Models.UnitOfMeasure();

            model.UnitId = entity.id;
            model.Text = entity.get("Text");
            model.Description = entity.get("Description");

            return model;
        }
    }
}
