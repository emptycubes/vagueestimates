/**
 * Created by Jack on 7/6/2014.
 */
/// <reference path="../../typings/parse/parse.d.ts" />
/// <reference path="../models/EstimateModel.ts" />
/// <reference path="MemberAdapter.ts" />

module VagueEstimates.Adapters {
    export class EstimateAdapter {

        public static ToViewModel(entity:libParse.IObject, members:Array<libParse.IObject>):Models.EstimateModel {
            var estimate = new Models.EstimateModel();

            estimate.EstimateId = entity.id;
            estimate.ProjectName = entity.get("ProjectName");
            estimate.ProductOwner = entity.get("ProductOwner");
            estimate.Deadline = entity.get("Deadline");
            estimate.Members = members.select(MemberAdapter.ToViewModel);

            return estimate;
        }

        public static ToEntity(viewModel:Models.EstimateModel, members:Array<libParse.IObject>, entity:libParse.IObjectStatic):libParse.IObject {
            var estimate = new entity();

            estimate.id = viewModel.EstimateId;
            estimate.set("ProjectName", viewModel.ProjectName);
            estimate.set("ProductOwner", viewModel.ProductOwner);
            estimate.set("DeadLine", viewModel.Deadline);
            estimate.relation(VagueEstimates.__MEMBER_ENTITY)
                    .add(members);

            return estimate;
        }
    }
}
